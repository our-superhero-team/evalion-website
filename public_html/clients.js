clients = {
    nestle : {
        text : "We strive to implement in a consistent and structured way, valid and innovative assessment procedures, following market trends and developments, as well as candidates’ expectations. Talent Simulations are a modern assessment method that quickly provides us with a representative image of a candidate’s potential in relation to specific professional behaviors.",
        attribution : "nestlé hr",
        position : "",
        logo : "nestle.svg"
    },
    ote : {
        text : "It is important to feel that you have partners who understand your business needs and possess the expertise to provide you with scientific and reliable solutions. These solutions lead to the identification and further professional development of talented employees, who have the potential to implement the future strategy of OTE Group. ",
        attribution : "Maria Georgiadi",
        position : "Performance & Talent Management Senior Manager",
        logo : "ote.svg"
    },
    actionaid : {
        logo : "actionaid.png"
    },
    alfabeta : {
        logo : "alfabeta.svg"
    },
    aims : {
        logo : "aims.svg"
    },
    alliance : {
        logo : "alliance.png"
    },
    allianz : {
        logo : "allianz.svg"
    },
    angelini : {
        logo : "angelini.png"
    },
    artion : {
        logo : "artion.png"
    },
    astrazeneca : {
        logo : "astrazeneca.svg"
    },
    athensExchange : {
        logo : "athensExchange.png"
    },
    aueb : {
        logo : "aueb.png"
    },
    byron : {
        logo : "byron.png"
    },
    casadipatsi : {
        logo : "casadipatsi.png"
    },
    chiesi : {
        logo : "chiesi.jpg"
    },
    colgpalm : {
        logo : "colgpalm.svg"
    },
    cosmoteEvalue : {
        logo : "cosmoteEvalue.png"
    },
    cyprusBank : {
        logo : "cyprusBank.png"
    },
    cyprusPolice : {
        logo : "cyprusPolice.svg"
    },
    cyprusUni : {
        logo : "cyprusUni.png"
    },
    ekDouka : {
        logo : "ekDouka.png"
    },
    eletson : {
        logo : "eletson.png"
    },
    ellhnogermanikh : {
        logo : "ellhnogermanikh.png"
    },
    elpedison : {
        logo : "elpedison.png"
    },
    eurolife : {
        logo : "eurolife.png"
    },
    genesis : {
        text : "Evalion is a company that truly supports children in taking the first steps in their career orientation. The company’s specialized executives, consistently and in an organized way, have actively contributed to fully informing our children about the field they have each selected to pursue. We greatly thank them for their support, as well as for the excellent professional cooperation that we maintain.",
        attribution : "Despina Proiou",
        position : "Human Resources Manager",
        logo : "genesis.png"
    },
    germanos : {
        logo : "germanos.png"
    },
    gsc : {
        logo : "gsc.png"
    },
    helic : {
        logo : "helic.png"
    },
    hellasgold : {
        logo : "hellasgold.jpg"
    },
    hellenicBank : {
        logo : "hellenicBank.png"
    },
    hellenicpetr : {
        text : "Hellenic Petroleum is a dynamic organization and one of the leading groups in the field of petroleum products and energy, not just in Greece, but in the wider region of SE Europe. The continuous transformation and development of the Group, creates complex needs for Human Resources Management, an area which we value greatly, as we consider investment in our staff to be the future of the Company. Transparency and Meritocracy are key principles governing the operations of Hellenic Petroleum, which is manifested in the organization of recruitment assessments, with the purpose of filling vacancies in our Company. In order to ensure the reliability of the results of the process, we work with a specialized company of Occupational Psychologists, EVALION. We use psychometric tests for Employee Selection, which enable us to select, in an objective and reliable way, candidates with high potential, for filling technical staff positions with special requirements. EVALION has professionally and reliably carried through with the services they have undertaken, demonstrating over time that they are a reliable partner, providing high-level support, characterized by the friendliness, knowledge and experience of their staff. ",
        attribution : "Panayiota Tsekoura",
        position : "Head of the Group’s Work Relations Department",
        logo : "hellenicpetr.svg"
    },
    hellenicTzilalis : {
        logo : "hellenicTzilalis.png"
    },
    henkel : {
        logo : "henkel.svg"
    },
    hertz : {
        logo : "hertz.svg"
    },
    icts : {
        text : "Our experience with the 360 assessment platform has been very positive because it enabled us to fully utilize our evaluation data’s maximum potential, faster, more reliable and with confidentiality. And we really appreciated the scientific approach and structure of the tool. In short, if you want to see beyond the data and you are looking to positively and directly impact the creation & delivery of performance development strategy, definitely, take a look at Evalion’s 360 feedback platform.",
        attribution : "Anastasia Asimakopoulou",
        position : "Organizational Development Manager",
        logo : "icts.png"
    },
    idealStandard : {
        logo : "idealStandard.png"
    },
    ionios : {
        logo : "ionios.png"
    },
    janssen : {
        logo : "janssen.jpeg"
    },
    jeannedarc : {
        logo : "jeannedarc.png"
    },
    johnson : {
        logo : "johnson.png"
    },
    kariera : {
        logo : "kariera.png"
    },
    leadonboard : {
        logo : "leadonboard.png"
    },
    leonteios : {
        logo : "leonteios.png"
    },
    maristes : {
        logo : "maristes.png"
    },
    metropolitancollege : {
        text : "The Metropolitan College department of Psychology is collaborating with Evalion to provide specialized training in psychometric tools for postgraduate students. Evalion has been selected for the high standards that it maintains at all stages of the psychometric assessment process.",
        attribution : "Dr Vicky Goltzi",
        position : "Head of Psychology",
        logo : "metropolitancollege.png"
    },
    moraiti : {
        logo : "moraiti.png"
    },
    mpaharika : {
        logo : "mpaharika.png"
    },
    mytilineos : {
        logo : "mytilineos.png"
    },
    nbg : {
        logo : "nbg.svg"
    },
    ogilvy : {
        logo : "ogilvy.jpg"
    },
    othisi : {
        logo : "othisi.png"
    },
    panagiotopoulos : {
        logo : "panagiotopoulos.png"
    },
    pangraph : {
        logo : "pangraph.png"
    },
    peoplecert : {
        logo : "peoplecert.svg"
    },
    photiades : {
        logo : "photiades.svg"
    },
    piraeusBank : {
        logo : "piraeusBank.svg"
    },
    randstad : {
        logo : "randstad.svg"
    },
    rsm : {
        logo : "rsm.png"
    },
    sj : {
        logo : "sj.png"
    },
    soulPublishing : {
        logo : "soulPublishing.png"
    },
    stoiximan : {
        logo : "stoiximan.svg"
    },
    teikav : {
        logo : "teikav.png"
    },
    titan : {
        text : "Assessment and Development Centers are a particular useful aid for us, enabling us to select the most suitable candidates for our positions, greatly reducing the possibility of a wrong and thus unjust – for both sides- decision. With the objective and valid assessment procedures that they implement, they pave the way, allowing us to quickly identify and capitalize on the candidates’ strengths and development prospects.",
        attribution : "Sofia Konstantinidou",
        position : "HR Development Manager",
        logo : "titan.svg"
    },
    unilever : {
        logo : "unilever.svg"
    },
    uom : {
        logo : "uom.png"
    },
    vib : {
        logo : "vib.png"
    },
    xml : {
        logo : "xml.svg"
    },
}

var randomProperty = function (obj) {
    var keys = Object.keys(obj);
    key = keys[ keys.length * Math.random() << 0];
    obj[key].name = key;
    return obj[key];
};
